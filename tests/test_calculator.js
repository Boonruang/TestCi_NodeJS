const Calculator = require('../src/calculator.js')
var assert = require('assert');

describe('calculator', function() {
    const calculator = new Calculator()
    it('should return 2 when the value is plus', function() {
        assert.equal(2, calculator.Plus(1,1));
    });
    it('should return 0 when the value is minus', () => {
        assert.equal(0, calculator.Minus(1,1));
    })
});
